import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = "earellano"
  password = ""
  errormessage = "Invalid user"
  invalidlogin = false

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  handlelogin() {
    console.log(this.username);
    if (this.username === "earellano") {
      this.invalidlogin = false
      this.router.navigate(['welcome', this.username])
    } else {
      this.invalidlogin = true      
    }
  }

}
