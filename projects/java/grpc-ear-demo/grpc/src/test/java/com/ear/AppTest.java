package com.ear;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest 
{
    @Autowired
    private GrpcClient grpcClient;

    @Test
    public void sayHelloTest()
    {
        assertThat(grpcClient.sayHello("Edmundo", "Arellano"), Matchers.equalTo("Hello, Edmundo Arellano"));
    }
}
