package com.ear;

import javax.annotation.PostConstruct;

import com.ear.grpc.HelloRequest;
import com.ear.grpc.HelloResponse;
import com.ear.grpc.HelloServiceGrpc;

import org.springframework.stereotype.Component;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

@Component
public class GrpcClient {

    private HelloServiceGrpc.HelloServiceBlockingStub stub;

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8082)
            .usePlaintext()
            .build();
        HelloServiceGrpc.HelloServiceBlockingStub stub = HelloServiceGrpc.newBlockingStub(channel);
        HelloResponse helloResponse = stub.hello(HelloRequest.newBuilder()
          .setFirstName("Edmundo")
          .setLastName("Arellano")
          .build());
        System.out.println("Respuesta: " + helloResponse.getGreeting());
        channel.shutdown();
    }

    public String sayHello(String firstName, String lastName) {         
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8082)
            .usePlaintext()
            .build();
        HelloServiceGrpc.HelloServiceBlockingStub stub = HelloServiceGrpc.newBlockingStub(channel);
        
        HelloResponse helloResponse = stub.hello(HelloRequest.newBuilder()
          .setFirstName(firstName)
          .setLastName(lastName)
          .build());
        System.out.println("Respuesta: " + helloResponse.getGreeting());
        channel.shutdown();
        return helloResponse.getGreeting();
    }
}