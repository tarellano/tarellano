package com.baeldung.dddmodules.sharedkernel.policy;

@FunctionalInterface
interface ApplicationPolicy<AgregatorOne> {
    PolicyResponse apply(AgregatorOne agregatorOne);
}

@FunctionalInterface
interface ApplicationPolicyBinary<AgregatorOne, AgregatorTwo> {
    PolicyResponse apply(AgregatorOne agregatorOne, AgregatorTwo agregatorTwo);
}

interface  PolicyResponse {}

class Allowance implements PolicyResponse { }

class Rejection implements PolicyResponse {

    static class Reason {
        String reason;

        private Reason(String reason) {
            this.reason = reason;
        }
    }

    Reason reason;

    private Rejection(Reason reason) {
        this.reason = reason;
    }

    static Rejection withReason(String reason) {
        return new Rejection(new Reason(reason));
    }
}