package programming.stagetwo;

@FunctionalInterface
public interface FuncInterface<A, B> {
    public Object doSomething(A a, B b);
}