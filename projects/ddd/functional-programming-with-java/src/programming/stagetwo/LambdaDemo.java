package programming.stagetwo;

public class LambdaDemo {

    public static void main(String[] args) {
        MyRunnable runnable = new MyRunnable();
        Thread t = new Thread(runnable);
        t.start();

        // using lambda's
        Thread t2 = new Thread(() -> System.out.println("it works"));
        t2.start();

        // functional interfcae
        MyFuncInterface func = () -> System.out.println("hola");
        func.myMethod();

        //funtional interface demo dos
        demoFunc(() -> System.out.println("hola dos"));
    }

    public static void demoFunc(MyFuncInterface func) {
        func.myMethod();
    }

}