package programming.stagetwo;

import java.util.function.Predicate;

public class DemoApp {

    public static void main(String[] args) {
        FuncInterface<String, String> func = (a, b) -> a+b;
        String c = (String) func.doSomething("a", "b");
        System.out.println(c);

        Predicate<String> funcPredicate = a -> a.length()>10?true:false;
        System.out.println(funcPredicate.test("Hola como"));
    }
    
}