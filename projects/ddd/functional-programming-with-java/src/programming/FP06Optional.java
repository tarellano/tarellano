package programming;

import java.util.Optional;

public class FP06Optional {

    public static void main(String[] args) {
        String nombre = null;
        Optional<String> oNombre = Optional.ofNullable(nombre);
        if (oNombre.isPresent()) {
            System.out.println(oNombre);
        }

        Optional<String> opt = Optional.of("test");
        opt.ifPresent(name -> System.out.println(name));

        String demoA = null;
        String demoB = "Something";
        System.out.println(Optional.ofNullable(demoA).orElse(demoB).toString());
    }
    
}
