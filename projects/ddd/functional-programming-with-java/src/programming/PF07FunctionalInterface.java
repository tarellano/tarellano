package programming;

import java.util.List;

@FunctionalInterface
interface ApplicationPolicy {
    Boolean apply(Order order);
}

public class PF07FunctionalInterface {

    private static List<ApplicationPolicy> currentPolicies;
    private static Order order = new Order("Test", 5);

    public static void main(String[] args) {
        
        ApplicationPolicy applicationPolicy = order -> order.totalAmount<10;
        addPolicy(applicationPolicy);
        applyPolicies();        
    }

    public static void addPolicy(ApplicationPolicy applicationPolicy) {
        currentPolicies = List.of(applicationPolicy);
    }

    public static void applyPolicies() {
        Object[] responses = currentPolicies.stream().map(policy -> policy.apply(order)).toArray();
        System.out.println("Ending...");
    }
    
}

class Order {
    String orderId;
    Integer totalAmount;

    Order(String orderId, Integer totalAmount) {
        this.orderId = orderId;
        this.totalAmount = totalAmount;
    }
}
