package programming;

class Store<T> {
    private T store;

    public void setStore(T t) {
        this.store = t;
    }
    public T getStore() {
        return this.store;
    }
    public String toString() {
        return this.store.toString();
    }

}

public class GenericsApp {
    public static void main(String[] args) {

        String message = "Demo message";
        Store<String> store = new Store<>();

        store.setStore(message);
        System.out.println(store.toString());

        Integer number = 10;
        Store<Integer> storeInt = new Store<>();
        storeInt.setStore(number);
        System.out.println(storeInt.toString());

    }
}