package programming;

import java.util.List;
import java.util.ServiceLoader;

class Quote {
    private String currency;
}

interface QuoteManager {
    List<Quote> getQuotes(String baseCurrency);
}


public class FP08ServiceLoader {
 
    public static void main(String[] args) {
        QuoteManager loader = ServiceLoader.load(QuoteManager.class).findFirst().get();
        //TO BE reviewed, not completed 
    }
    
}