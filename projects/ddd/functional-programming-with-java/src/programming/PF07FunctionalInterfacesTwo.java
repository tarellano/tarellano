package programming;

import java.util.List;

/*
 Functional interface definition could use NON existing classes as parameters, ex. AgregatorOne and AgregatorTwo
*/
@FunctionalInterface
interface ApplicationPolicyBi<AgregatorOne, AgregatorTwo>{
    PolicyResponse myapply(AgregatorOne agregatorOne, AgregatorTwo agregatorTwo);
}

@FunctionalInterface
interface Foo {
    String method(String name);
}

public class PF07FunctionalInterfacesTwo {

    // next variable stores a set of policies applied to two entities/agregators
    private static List<ApplicationPolicyBi<OrderTwo, Shipping>> currentPolicies;
    private static OrderTwo orderTwoObject = new OrderTwo("Test", 5);
    private static Shipping shippingObject = new Shipping("shipping", 5);

    public static void main(String[] args) {
        
        // when rule is defined, rules are defined over classes and not over objects
        ApplicationPolicyBi<OrderTwo, Shipping> applicationPolicy = (orderTwo, shipping) -> { 
                if (orderTwo.totalAmount>10 || shipping.totalAmount >10) {
                    return Rejection.withReason("Order or Shipping over limit");             
                } else {
                    return new Allowance();
                }
        };
        addPolicy(applicationPolicy);
        // this methos apply pilicies over objects
        applyPolicies();


        System.out.println(scopeExperiment());
    }

    public static void addPolicy(ApplicationPolicyBi<OrderTwo, Shipping> applicationPolicy) {
        currentPolicies = List.of(applicationPolicy);
    }

    public static void applyPolicies() {
        Object[] responses = currentPolicies.stream().map(policy -> policy.myapply(orderTwoObject, shippingObject)).toArray();
        System.out.println("Ending...");
    }

    /***************  Other samples  *****************/

    public static String scopeExperiment() {
        System.out.println("************  Shiow how not to override functional interfaces **************");

        Foo fooIC = new Foo() {
            String value = "Inner class value";
            @Override
            public String method(String string) {
                return this.value;
            }
        };
        String resultIC = fooIC.method("something");
        System.out.println(resultIC);
        
        Foo fooLambda = parameter -> {
            String value = "Lambda value";
            return value;
        };
        String resultLambda = fooLambda.method("other something");
        return "Results: resultIC = " + resultIC + ", resultLambda = " + resultLambda;
    }





}

class OrderTwo {
    String orderId;
    Integer totalAmount;

    OrderTwo(String orderId, Integer totalAmount) {
        this.orderId = orderId;
        this.totalAmount = totalAmount;
    }
}

class Shipping {
    String shippingId;
    Integer totalAmount;

    Shipping(String shippingId, Integer totalAmount) {
        this.shippingId = shippingId;
        this.totalAmount = totalAmount;
    }
}


interface PolicyResponse {
}

class Allowance implements PolicyResponse {
}

class Rejection implements PolicyResponse {

    static class Reason {
        String reason;

        private Reason(String reason) {
            this.reason = reason;
        }
    }

    Reason reason;

    Rejection(Reason reason) {
        this.reason = reason;
    }

    static Rejection withReason(String reasonMessage) {
        return new Rejection(new Reason(reasonMessage));
    }
}