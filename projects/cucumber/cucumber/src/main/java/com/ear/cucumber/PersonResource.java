package com.ear.cucumber;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonResource {

    @Autowired
    private PersonJPARepository personJPARepository;

    @GetMapping("/people")
    public List<Person> getAllPeople(){
            return personJPARepository.findAll();
    }

}