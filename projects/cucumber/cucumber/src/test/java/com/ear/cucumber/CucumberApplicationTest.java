package com.ear.cucumber;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(
	classes = CucumberApplication.class,
	webEnvironment = WebEnvironment.DEFINED_PORT)
class CucumberApplicationTest {
	   
	@Test
	void contextLoads() {
	}	

}
