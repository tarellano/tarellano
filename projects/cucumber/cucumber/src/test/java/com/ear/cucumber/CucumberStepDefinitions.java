package com.ear.cucumber;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
  
public class CucumberStepDefinitions  extends CucumberApplicationTest {

    private HttpResponse latestResponse;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @When("^the client calls /version$")
	public void the_client_issues_GET_version() throws Throwable {
		this.executeGet("http://localhost:8080/version");
	}

	@Then("^the client receives status code of (\\d+)$")
	public void the_client_receives_status_code_of(int statusCode) throws Throwable {
        Integer currentStatusCode = new Integer(latestResponse.getStatusLine().getStatusCode());
        logger.info("currentStatusCode: " + currentStatusCode);
		assertTrue(currentStatusCode.intValue() == 200); 
	}

	@And("^the client receives server version (.+)$")
	public void the_client_receives_server_version_body(String version) throws Throwable {
        HttpEntity entity = latestResponse.getEntity();
        String responseString = EntityUtils.toString(entity, "UTF-8");
		assertTrue(responseString.equals("1.0"));     
    }

    @When("^the client calls /echo$")
    public void the_client_calls_echo() throws Throwable {
        this.executeGet("http://localhost:8080/echo");
    }

    @Then("^the client receives server message echo$")
    public void the_client_receives_server_message_echo() throws Throwable {
        HttpEntity entity = latestResponse.getEntity();
        String responseString = EntityUtils.toString(entity, "UTF-8");
		assertTrue(responseString.equals("echo"));
    }


	public void executeGet(String url) throws ClientProtocolException, IOException {
		HttpClient client = HttpClientBuilder.create().build();    
		this.latestResponse = client.execute(new HttpGet(url));
				
		//int statusCode = latestResponse.getStatusLine().getStatusCode();		
	} 

}