# generate service
protoc greet/greetpb/greet.proto --go_out=plugins=grpc:.

# execute server
go run greet/greet_server/server.go

# build binary executable file
go build greet/greet_server/server.go

# pubish on docker
docker build -t my-go-app .

# execute test recursively 
go test ./...



#######  other commands   ########
# ALL next command must to be executed into greet PATH
################################

# command to initialze compoment container
go mod init ear.com.mx/greet

# command to install server into $HOME/go/bin to be able to execute it directly from command line
go install ear.com/greet

# next command is useful to build/compile a GO file(s) without installing into BIN  path, normally used into packages path's
go build

# to remove all cache packages
go clean -modcache



