package main

import (
	"fmt"

	"ear.com.mx/goschool/goschoolutils"
)

func main() {
	// next three messages will appear at the end of the execution, first is last
	defer fmt.Println("defered message 1")
	defer fmt.Println("defered message 2")
	defer fmt.Println("\n**********  start defered messages ************")

	goschoolutils.Arithmetics()

	goschoolutils.Pointers()

	goschoolutils.Classes()

	goschoolutils.Functions()

	goschoolutils.Playfunc()

}
