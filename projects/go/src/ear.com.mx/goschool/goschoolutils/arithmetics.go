package goschoolutils

import (
	"fmt"
	"math/rand"
)

// Add function
func Add(x int, y int) (z int) {
	z = x + y
	return
}

// Arithmetics function
func Arithmetics() {
	fmt.Println("Hello, Edmundo")
	fmt.Println("My favorite number is", rand.Intn(10))
	fmt.Println("Suma 4 + 5: ", Add(4, 5))
}
