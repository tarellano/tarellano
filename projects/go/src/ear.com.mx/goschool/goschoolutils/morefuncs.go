package goschoolutils

import "fmt"

// Person demo struct
type Person struct {
	name string
	age  int
}

// Birthdate demo func
func (p *Person) Birthdate() func() string {
	p.age++
	return func() string {
		return "Happy bithdate " + p.name
	}
}

// BirthdateFunc demo func
func BirthdateFunc(p *Person) {
	p.age++
}

// Playfunc demo functions
func Playfunc() {
	fmt.Println("\n***********  START demo advanced functions  ************")
	p := Person{"Edmundo, Arellano", 44}
	message := p.Birthdate()
	BirthdateFunc(&p)

	v := &Person{"Otra, Persona", 40}
	secondmessage := v.Birthdate()
	BirthdateFunc(v)

	fmt.Println(v, p, message(), secondmessage())
}
