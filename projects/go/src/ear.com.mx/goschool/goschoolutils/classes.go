package goschoolutils

import "fmt"

// Author structure
type Author struct {
	name      string
	branch    string
	particles int
	salary    int
}

// Method with a receiver
// of author type
func (a Author) show() {
	fmt.Println("\n\n**********  Show author content *****************:")
	fmt.Println("Author's Name: ", a.name)
	fmt.Println("Branch Name: ", a.branch)
	fmt.Println("Published articles: ", a.particles)
	fmt.Println("Salary: ", a.salary)
}

// Classes struct addin methods
func Classes() {
	// agregar metodos a una estructura, es como si una estructura actuara como una clase
	// Initializing the values
	// of the author structure
	res := Author{
		name:      "Sona",
		branch:    "CSE",
		particles: 203,
		salary:    34000,
	}

	// Calling the method
	res.show()
}
