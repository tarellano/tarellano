package goschoolutils

import (
	"fmt"
)

func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func(int, int) (int, int) {
	prev, next := 0, 0
	return func(a int, b int) (int, int) {
		prev = a
		next = next + b
		return prev, next
	}
}

// Functions demos
func Functions() {
	fmt.Println("\n **************** Start with advanced functions demos ***************** ")
	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}

	fmt.Println("\nFobonacci demo")
	fibo := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(fibo(i, i))
	}

}
