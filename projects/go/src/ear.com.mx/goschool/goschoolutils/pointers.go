package goschoolutils

import (
	"fmt"
)

// Vertex not to be exported
type Vertex struct {
	X int
	Y int
}

// Pointers sample
func Pointers() {
	fmt.Println("\n********* start pointers samples  ***********")
	i, j := 42, 2701
	p := &i         // point to i
	fmt.Println(*p) // read i through the pointer
	*p = 21         // set i through the pointer
	fmt.Println(i)  // see the new value of i

	p = &j         // point to j
	*p = *p / 37   // divide j through the pointer
	fmt.Println(j) // see the new value of j

	fmt.Println("\n********* start struct smpls  ***********")
	v := Vertex{1, 2}
	p2 := &v
	p2.X = 1e9
	fmt.Println(v)
}
