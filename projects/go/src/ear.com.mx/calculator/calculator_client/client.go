package main

import (
	"context"
	"fmt"
	"log"

	"ear.com.mx/calculator/calculatorpb"

	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Hello this is the client")
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := calculatorpb.NewSumServiceClient(cc)
	//fmt.Printf("Created client: %f", c)

	doUnary(c)
}

func doUnary(c calculatorpb.SumServiceClient) {
	fmt.Println("Starting to do UNARY RPC")
	req := &calculatorpb.SumRequest{
		Sum: &calculatorpb.Sum{
			FirstValue:  10,
			SecondValue: 13,
		},
	}
	res, err := c.Sum(context.Background(), req)
	if err != nil {
		log.Fatalf("Error when calling SUM RPC: %v", err)
	}
	log.Printf("Response from SUM: %d", res.Result)
}
