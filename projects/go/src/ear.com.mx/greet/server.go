package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"time"

	"ear.com.mx/greet/greetpb"
	"ear.com.mx/greet/greetutils"

	"google.golang.org/grpc"
)

type server struct{}

func (*server) Greet(ctx context.Context, req *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	fmt.Printf("Greet function was invoked with: %v\n", req)
	firstName := req.GetGreeting().GetFirstName()
	result := "Hello " + firstName

	res := &greetpb.GreetResponse{
		Result: result,
	}
	return res, nil
}

func (*server) GreetManyTimes(req *greetpb.GreetManyTimesRequest, stream greetpb.GreetService_GreetManyTimesServer) error {
	fmt.Printf("GreetManyTimes function was invoked with: %v\n", req)
	greetutils.PrimeNumbers(240)
	firstName := req.GetGreeting().GetFirstName()
	for i := 0; i < 10; i++ {
		result := "Hello " + firstName + " Number " + strconv.Itoa(i)
		res := &greetpb.GreetManyTimesResponse{
			Result: result,
		}
		stream.Send(res)
		time.Sleep(1000 * time.Millisecond)
	}
	return nil
}

func (*server) PrimeNumbers(req *greetpb.PrimeNumbersRequest, stream greetpb.GreetService_PrimeNumbersServer) error {
	fmt.Printf("PrimeNumbers function was invoked with: %v\n", req)

	numberParam, err := strconv.Atoi(req.GetNumberParam())
	if err != nil {
		log.Println("Error getting input parameter")
	}
	primeNumbersResult := greetutils.PrimeNumbers(numberParam)
	for i := 0; i < len(primeNumbersResult); i++ {
		result := primeNumbersResult[i]
		fmt.Println("result from primeNumbers func calcul: ", result)
		res := &greetpb.PrimeNumbersResponse{
			Result: strconv.Itoa(result),
		}
		stream.Send(res)
		time.Sleep(1000 * time.Millisecond)
	}
	return nil
}

func (*server) LongGreet(stream greetpb.GreetService_LongGreetServer) error {
	fmt.Println("\nLongGreet functions was invoked for client streaming")
	result := "Hello"
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			// finish for client reading
			return stream.SendAndClose(&greetpb.LongGreetResponse{
				Result: result,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading streaming: %v", err)
		}
		firstName := req.GetGreeting().GetFirstName()
		result += firstName + "!"
	}

}

func main() {
	fmt.Println("Hello world!")

	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("Failed to listen %v", err)
	}

	s := grpc.NewServer()
	greetpb.RegisterGreetServiceServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to server: %v", err)
	}

}
