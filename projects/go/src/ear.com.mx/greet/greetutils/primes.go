package greetutils

import "fmt"

// by given a number return prime numbers
func PrimeNumbers(numberParameter int) []int {
	fmt.Println("Entering primeNumbers func calcul")
	k := 2
	i := 0
	var response []int
	for numberParameter > 1 {
		if numberParameter%k == 0 {
			numberParameter = numberParameter / k
			response = append(response, numberParameter)
			fmt.Println(numberParameter)
			i++
		} else {
			k = k + 1
		}
	}
	fmt.Println("Ends primeNumbers func")
	return response
}
